/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file openMP_Exoteric.hpp

    @brief  The implementation details of Exoteric Calculations using MPI+OpenMP
*/
#include <omp.h>
#include <vector>
#include "Block.h"
#include "NeighProcesses.hpp"

#define thread_count 4
/*
  @brief Helper host Function for calculating the Angle Cells using hybrid mpi+openMP
  
  @param[in]blockArrayNext : Block of the next Generation
  @param[in]blockArrayCurrent : Block of the current Generation
  @param[in]topLeft: rank of the top Left neighbour process.
  @param[in]botLeft: rank of the bottom Left neighbour process.
  @param[in]topRight: rank of the top Right neighbour process.
  @param[in]botRight: rank of the bottom Right neighbour process.
  @param[in] topRow: this is the Row from the Upper Neighbour Process. 
  @param[in] botRow: this is the Row from the bottom Neighbour Process. 
  @param[in] leftCol: this is the Row from the left Neighbour Process. 
  @param[in] rightCol: this is the Row from the right Neighbour Process. 

*
inline void calculate4SingleAngleCells(std::vector<std::vector<int>> &blockArrayNext, Block &blockArrayCurrent,
                                       int topLeft, int botLeft, int topRight, int botRight,
                                       std::vector<int> topRow, std::vector<int> botRow, std::vector<int> leftCol, std::vector<int> rightCol)
{

    int aliveNeighbours = 0;
    int cellsperblockRow = blockArrayCurrent.bl.noofcols;
    int cellsperblockCol = blockArrayCurrent.bl.noofrows;

    int rank; //, np, iam;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

#pragma omp parallel // starts a new team
    {

#pragma omp sections // divides the team into sections
        {
#pragma omp section
            {

                //We calculate the topLeft !
                aliveNeighbours = topRow[0] + topRow[1] + topLeft + leftCol[0] + leftCol[1] + blockArrayCurrent.blockArray[0][1] + blockArrayCurrent.blockArray[1][1] + blockArrayCurrent.blockArray[1][0];
                if (blockArrayCurrent.blockArray[0][0] == 1)
                    blockArrayNext[0][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
                else
                {
                    blockArrayNext[0][0] = aliveNeighbours == 3 ? 1 : 0;
                }

                // std::cout << "[0][0]:::" << blockArrayNext[0][0] << "rank" << rank << std::endl;

                //np = omp_get_num_threads();
                //iam = omp_get_thread_num();
                // printf("Hello from thread %d out of %d from process %d value::[0][0]:: %d\n",
                //            iam, np, rank,  blockArrayNext[0][0]);
            }
#pragma omp section
            {
                aliveNeighbours = 0;
                //We calculate the topRight !
                aliveNeighbours = topRow[cellsperblockRow - 1] + topRow[cellsperblockRow - 2] + topRight + rightCol[0] + rightCol[1] + blockArrayCurrent.blockArray[0][cellsperblockRow - 2] + blockArrayCurrent.blockArray[1][cellsperblockRow - 1] + blockArrayCurrent.blockArray[1][cellsperblockRow - 2];
                if (blockArrayCurrent.blockArray[0][cellsperblockRow - 1] == 1)
                    blockArrayNext[0][cellsperblockRow - 1] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
                else
                {
                    blockArrayNext[0][cellsperblockRow - 1] = aliveNeighbours == 3 ? 1 : 0;
                }

                //std::cout << " [0][cellsperblockRow - 1]:::  "<< blockArrayNext[0][cellsperblockRow - 1] << "rank" << rank << std::endl;

                //np = omp_get_num_threads();
                //iam = omp_get_thread_num();
                // printf("Hello from thread %d out of %d from process %d value:: [0][cellsperblockRow - 1]: %d\n",
                // iam, np, rank,blockArrayNext[0][cellsperblockRow - 1]);
            }
#pragma omp section
            {
                aliveNeighbours = 0;
                //We calculate the botLeft !
                aliveNeighbours = botRow[0] + botRow[1] + botLeft + leftCol[cellsperblockCol - 1] + leftCol[cellsperblockCol - 2] + blockArrayCurrent.blockArray[cellsperblockCol - 1][1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][0];
                if (blockArrayCurrent.blockArray[cellsperblockCol - 1][0] == 1)
                    blockArrayNext[cellsperblockCol - 1][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
                else
                {
                    blockArrayNext[cellsperblockCol - 1][0] = aliveNeighbours == 3 ? 1 : 0;
                }

                //std::cout << " [cellsperblockCol - 1][0]:::: " << blockArrayNext[cellsperblockCol - 1][0] << "rank" << rank <<std::endl;

                //np = omp_get_num_threads();
                //iam = omp_get_thread_num();
                // printf("Hello from thread %d out of %d from process %d value:: [cellsperblockCol - 1][0] %d\n",
                // iam, np, rank, blockArrayNext[cellsperblockCol - 1][0]);
            }
#pragma omp section
            {
                aliveNeighbours = 0;
                //We calculate the botRight
                aliveNeighbours = botRow[cellsperblockRow - 1] + botRow[cellsperblockRow - 2] + botRight + rightCol[cellsperblockCol - 1] + rightCol[cellsperblockCol - 2] + blockArrayCurrent.blockArray[cellsperblockCol - 1][cellsperblockRow - 2] + blockArrayCurrent.blockArray[cellsperblockCol - 2][cellsperblockRow - 1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][cellsperblockRow - 2];
                if (blockArrayCurrent.blockArray[cellsperblockCol - 1][cellsperblockRow - 1] == 1)
                    blockArrayNext[cellsperblockCol - 1][cellsperblockRow - 1] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
                else
                {
                    blockArrayNext[cellsperblockCol - 1][cellsperblockRow - 1] = aliveNeighbours == 3 ? 1 : 0;
                }

                //std::cout << " [cellsperblockCol - 1][cellsperblockRow - 1]::: " <np = omp_g// }}
            }
        }
    }
}
*/

/*
  @brief Host Function for exoteric Calculation using hybrid MPI+ OpenMP

  @param[in]blockArrayNext : Block of the next Generation as a result
  @param[in]blockArrayCurrent : Block of the current Generation
  @param[in]NPs : details of the Neighbour Processes
*/
inline void openMP_exotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &Bl, NeighProcesses &NPs)
{
    int np = omp_get_num_threads();
    int iam = omp_get_thread_num();
#pragma omp parallel default(shared) private(iam, np)
    {
        int aliveNeighbours = 0;
        int cellsperblockRow = Bl.bl.noofrows;
        int cellsperblockCol = Bl.bl.noofcols;

        int temp = cellsperblockRow;
        cellsperblockRow = cellsperblockCol;
        cellsperblockCol = temp;

        std::vector<int> topRow = NPs.rcvdFromUpperNeigh;
        std::vector<int> botRow = NPs.rcvdFromDownNeigh;
        std::vector<int> leftCol = NPs.rcvdFromLeftNeigh;
        std::vector<int> rightCol = NPs.rcvdFromRightNeigh;
        int topLeft = NPs.upLeftSingle, botLeft = NPs.downLeftSingle, topRight = NPs.upRightSingle, botRight = NPs.downRightSingle;
#pragma omp barrier
#pragma omp master
        aliveNeighbours = topRow[0] + topRow[1] + topLeft + leftCol[0] + leftCol[1] + Bl.blockArray[0][1] + Bl.blockArray[1][1] + Bl.blockArray[1][0];
        if (Bl.blockArray[0][0] == 1)
            blockArrayNext[0][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[0][0] = aliveNeighbours == 3 ? 1 : 0;
        }
        //cout << "NEO: " << blockArrayNext[0][0] << "****** ME TOSOUS ZWNTANOUS: " << aliveNeighbours << endl;
        aliveNeighbours = 0;

        //We calculate the topRight !
        aliveNeighbours = topRow[cellsperblockRow - 1] + topRow[cellsperblockRow - 2] + topRight + rightCol[0] + rightCol[1] + Bl.blockArray[0][cellsperblockRow - 2] + Bl.blockArray[1][cellsperblockRow - 1] + Bl.blockArray[1][cellsperblockRow - 2];
        if (Bl.blockArray[0][cellsperblockRow - 1] == 1)
            blockArrayNext[0][cellsperblockRow - 1] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[0][cellsperblockRow - 1] = aliveNeighbours == 3 ? 1 : 0;
        }
        //cout << "NEO: " << blockArrayNext[0][cellsperblockRow -1] << "****** ME TOSOUS ZWNTANOUS: " << aliveNeighbours << endl;
        aliveNeighbours = 0;

        //We calculate the botLeft !
        aliveNeighbours = botRow[0] + botRow[1] + botLeft + leftCol[cellsperblockCol - 1] + leftCol[cellsperblockCol - 2] + Bl.blockArray[cellsperblockCol - 1][1] + Bl.blockArray[cellsperblockCol - 2][1] + Bl.blockArray[cellsperblockCol - 2][0];
        if (Bl.blockArray[cellsperblockCol - 1][0] == 1)
            blockArrayNext[cellsperblockCol - 1][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[cellsperblockCol - 1][0] = aliveNeighbours == 3 ? 1 : 0;
        }
        //cout << "NEO: " << blockArrayNext[cellsperblockCol -1][0] << "****** ME TOSOUS ZWNTANOUS: " << aliveNeighbours << endl;
        aliveNeighbours = 0;

        //We calculate the botRight
        aliveNeighbours = botRow[cellsperblockRow - 1] + botRow[cellsperblockRow - 2] + botRight + rightCol[cellsperblockCol - 1] + rightCol[cellsperblockCol - 2] + Bl.blockArray[cellsperblockCol - 1][cellsperblockRow - 2] + Bl.blockArray[cellsperblockCol - 2][cellsperblockRow - 1] + Bl.blockArray[cellsperblockCol - 2][cellsperblockRow - 2];
        if (Bl.blockArray[cellsperblockCol - 1][cellsperblockRow - 1] == 1)
            blockArrayNext[cellsperblockCol - 1][cellsperblockRow - 1] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[cellsperblockCol - 1][cellsperblockRow - 1] = aliveNeighbours == 3 ? 1 : 0;
        }
        //cout << "NEO: " << blockArrayNext[cellsperblockCol - 1][cellsperblockRow - 1] << "****** ME TOSOUS ZWNTANOUS: " << aliveNeighbours << endl;
        aliveNeighbours = 0;

#pragma omp barrier
#pragma omp parallel for num_threads(thread_count) default(shared) reduction(+ \
                                                                             : aliveNeighbours)
        //We calculate the top inbetween
        for (int i = 1; i <= cellsperblockRow - 2; i++)
        {
            aliveNeighbours = 0;
            aliveNeighbours = topRow[i - 1] + topRow[i] + topRow[i + 1] + Bl.blockArray[0][i - 1] + Bl.blockArray[0][i + 1] + Bl.blockArray[1][i - 1] + Bl.blockArray[1][i] + Bl.blockArray[1][i + 1];
            if (Bl.blockArray[0][i] == 1)
                blockArrayNext[0][i] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
            else
            {
                blockArrayNext[0][i] = aliveNeighbours == 3 ? 1 : 0;
            }
        }

        //We calculate the bot inbetween
        for (int i = 1; i <= cellsperblockRow - 2; i++)
        {
            aliveNeighbours = 0;
            aliveNeighbours = botRow[i - 1] + botRow[i] + botRow[i + 1] + Bl.blockArray[cellsperblockCol - 1][i - 1] + Bl.blockArray[cellsperblockCol - 1][i + 1] + Bl.blockArray[cellsperblockCol - 2][i - 1] + Bl.blockArray[cellsperblockCol - 2][i] + Bl.blockArray[cellsperblockCol - 2][i + 1];
            if (Bl.blockArray[cellsperblockCol - 1][i] == 1)
                blockArrayNext[cellsperblockCol - 1][i] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
            else
            {
                blockArrayNext[cellsperblockCol - 1][i] = aliveNeighbours == 3 ? 1 : 0;
            }
        }

        //We calculate the left inbetween
        for (int i = 1; i <= cellsperblockCol - 2; i++)
        {
            aliveNeighbours = 0;
            aliveNeighbours = leftCol[i - 1] + leftCol[i] + leftCol[i + 1] + Bl.blockArray[i - 1][0] + Bl.blockArray[i + 1][0] + Bl.blockArray[i - 1][1] + Bl.blockArray[i + 1][1] + Bl.blockArray[i][1];
            if (Bl.blockArray[i][0] == 1)
                blockArrayNext[i][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
            else
            {
                blockArrayNext[i][0] = aliveNeighbours == 3 ? 1 : 0;
            }
        }

        //We calculate the right inbetween
        for (int i = 1; i <= cellsperblockCol - 2; i++)
        {
            aliveNeighbours = 0;
            aliveNeighbours = rightCol[i - 1] + rightCol[i] + rightCol[i + 1] + Bl.blockArray[i - 1][cellsperblockRow - 1] + Bl.blockArray[i + 1][cellsperblockRow - 1] + Bl.blockArray[i - 1][cellsperblockRow - 2] + Bl.blockArray[i + 1][cellsperblockRow - 2] + Bl.blockArray[i][cellsperblockRow - 2];
            if (Bl.blockArray[i][0] == 1)
                blockArrayNext[i][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
            else
            {
                blockArrayNext[i][0] = aliveNeighbours == 3 ? 1 : 0;
            }
        }
    }
}
