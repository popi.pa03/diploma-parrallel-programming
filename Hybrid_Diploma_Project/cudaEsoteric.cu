#include <stdio.h>
#include <cuda.h>
#include <math.h>
#include <cuda_runtime.h>
#include <iostream>


#include "Block.h"
#include "hybridCudaMPI.hpp"


#define CUDA_CALL(F)                                                          \
    if ((F) != cudaSuccess)                                                   \
    {                                                                         \
        printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
               __FILE__, __LINE__);                                           \
        exit(-1);                                                             \
    }



texture<int, cudaTextureType2D, cudaReadModeElementType> texRef;

__global__ void updatedValueGPU(int* prevBlock, int* currentBlock, int nRows, int nColumns)
{
	const unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	int xCoord;
	int yCoord;
	int lowX, lowY, maxX, maxY;
	int activeNeigh;

	if (i < nRows*nColumns)
	{
		xCoord = i / nColumns;
		yCoord = i - xCoord * nColumns;

		lowX = nColumns * ((xCoord - 1 + nRows) % nRows); //the up element on x axis, when -1 goes to nRows-1
		lowY = (yCoord - 1 + nColumns) % nColumns; //the up element on y axis, when -1 goes to nColumns-1

		maxX = nColumns * ((xCoord + 1) % nRows); //the down element on x axis,  when nRows goes to 0
		maxY = (yCoord + 1) % nColumns; //the down element on y axis

		activeNeigh = prevBlock[lowX + lowY] + prevBlock[lowX + yCoord] + prevBlock[lowX + maxY] //the up 3 neighbors
			+ prevBlock[xCoord*nColumns + lowY] + prevBlock[xCoord*nColumns + maxY] //the middle neighbors
			+ prevBlock[maxX + lowY] + prevBlock[maxX + yCoord] + prevBlock[maxX + maxY]; //the down 3 neighbors


		if (activeNeigh == 3 || (activeNeigh == 2 && prevBlock[xCoord*nColumns + yCoord] == 1)) //find new value of the cell
		{
			currentBlock[xCoord*nColumns + yCoord] = tex2D(texRef,1,1);
		}
		else
		{
			currentBlock[xCoord*nColumns + yCoord] = tex2D(texRef, 0, 0);
		}
	}

}

extern "C" void vecToArr(std::vector<std::vector<int> > &vals, int*& newArr)
{
	for (unsigned i = 0; (i < vals.size()); i++)
	{
		for (unsigned j = 0; (j < vals[i].size()); j++)
		{
			newArr[i* vals.size()+ j] = vals[i][j];
		}
	}
}

extern "C" void arrToVec(std::vector<std::vector<int> > &vals, int*& newArr)
{
	for (unsigned i = 1; (i < vals.size() - 2); i++)
	{
		for (unsigned j = 1; (j < vals[i].size() - 2); j++)
		{
			vals[i][j] = newArr[i * vals.size() + j];
		}
	}
}

extern "C" void gameOfLifeGPU(int* h_Block, int nRows, int nColumns)
{
	int memSize;
	const int BLOCK_SIZE = 256;
	int N = nRows * nColumns * sizeof(int);

	memSize = nRows * nColumns * sizeof(int);

	// Allocate CUDA array in device memory
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0,
		cudaChannelFormatKindUnsigned);

	cudaArray* cuArray;

	CUDA_CALL(cudaMallocArray(&cuArray, &channelDesc, memSize, 0 ,0));
	CUDA_CALL(cudaMemcpyToArray(cuArray, 0, 0, &h_Block, memSize, cudaMemcpyHostToDevice));

	// Set texture parameters
	texRef.addressMode[0] = cudaAddressModeWrap;
	texRef.addressMode[1] = cudaAddressModeWrap;
	texRef.filterMode = cudaFilterModeLinear;
	texRef.normalized = true;

	// Bind the array to the texture reference
	cudaBindTextureToArray(texRef, cuArray, channelDesc);

	int* d_currentBlock;
	CUDA_CALL(cudaMalloc(&d_currentBlock, memSize));
	int* h_Block2 = new int[nColumns*nRows];
	CUDA_CALL(cudaMalloc(&h_Block2, memSize));

  CUDA_CALL(cudaMemcpy(h_Block2, h_Block, memSize, cudaMemcpyHostToDevice));

	updatedValueGPU << < (N + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE  >> > (h_Block2, d_currentBlock, nColumns, nRows);

	CUDA_CALL(cudaMemcpy(h_Block, d_currentBlock, memSize, cudaMemcpyDeviceToHost));
	CUDA_CALL(cudaThreadSynchronize());

	CUDA_CALL(cudaFreeArray(cuArray));
	CUDA_CALL(cudaFree(d_currentBlock));

}

extern "C" void printBlock(std::vector<std::vector<int> >& blockVec){


std::cout << " BLOCK " <<std::endl;

for ( std::vector<std::vector<int> >::size_type i = 0; i < blockVec.size(); i++ )
{
   for ( std::vector<int>::size_type j = 0; j < blockVec[i].size(); j++ )
   {
      std::cout << blockVec[i][j] << ' ';
   }
   std::cout << std::endl;
}

}

extern "C" void printArr(int *& arr, int size){

	for(int i =0; i < size ; ++i){
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}


/*
    @brief  Host Function that allows the MPI process to set the CUDA device before the MPI environment is initialized.
            This is for both Cuda-aware MPI version and Normal one.
            For the Normal Version (without Cuda awareness) this will not do anything.
            For the CUDA-aware MPI version, the is the only place where the device gets set.
            To achieve this, we use the node's local rank, as the MPI environment has not been initialized yet.
*/
extern "C" void callSetDeviceBeforeInit()
{
    char *localRankStr = NULL;
    int rank = 0, devCount = 0;

    // We extract the local rank initialization using an environment variable
    if ((localRankStr = getenv(ENV_LOCAL_RANK)) != NULL)
    {
        rank = atoi(localRankStr);
    }

    CUDA_CALL(cudaGetDeviceCount(&devCount));
    CUDA_CALL(cudaSetDevice(rank % devCount));
}

/*
    @brief  Host Function that allows the MPI process to set the CUDA device after the MPI environment is initialized.
            This is for both Cuda-aware MPI version and Normal one.
            For the Normal Version (without Cuda awareness) we use the process rank as the MPI is initialized
            For the CUDA-aware MPI version, this will not affect anything.

    @param[in]  rank: Process Rank after Cartesian Topology
 */
extern "C" void callSetDeviceAfterInit(int rank)
{
    int devCount = 0;

    CUDA_CALL(cudaGetDeviceCount(&devCount));
    CUDA_CALL(cudaSetDevice(rank % devCount));
}

/*
    NOT COMPLETED
    @brief  Host Function that writes received results to Cuda Memory. This works only for Normal-Cuda-MPI

*/
extern "C"  void callExchangeData(int *dest, int *src, int byteCount, int transferOption)
{
    byteCount = byteCount * sizeof(int);
    if (transferOption == 1)
    {
        CUDA_CALL(cudaMemcpy(dest, src, byteCount, cudaMemcpyHostToDevice));
    }
    else
    {
        CUDA_CALL(cudaMemcpy(src, dest, byteCount, cudaMemcpyDeviceToHost));
    }
}


extern "C" void cuda_esotericCalculation(std::vector<std::vector<int> >& blockArrayNext, Block &BL)
{
	int * h_prevBlock;

	h_prevBlock = new int[BL.blockArray.size() * BL.blockArray[0].size()];

	vecToArr(BL.blockArray, h_prevBlock);

	gameOfLifeGPU(h_prevBlock, BL.blockArray.size(), BL.blockArray[0].size());

	arrToVec(blockArrayNext, h_prevBlock);

	delete[] h_prevBlock;
	h_prevBlock = NULL;
}
