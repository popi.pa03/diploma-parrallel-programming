#include <iostream>
#include <vector>
#include <cstring>
#include <fstream>
#include <sstream>
#include <algorithm>


using namespace std;


inline void printGenerations(std::vector<std::vector<int> >& Array, std::vector<std::vector<int> >& nextArray)
{
  auto print = [&](std::vector<std::vector<int> > grid) {
    for ( const std::vector<int> &v : grid )
    {
      for ( int x : v ) std::cout << x << ' ';
      std::cout << std::endl;
    }
  };

  cout << " CURRENT : " << endl;
  print(Array);
  cout << " NEXT: " << endl;
  print(nextArray);
}

void Terminate(std::vector<std::vector<int> >& Array, std::vector<std::vector<int> >& nextArray, int& toSend){
  std::vector<std::vector<int>> zeroVec(Array.size(), std::vector<int>(Array[0].size(), 0));
	cout << "in Term" ;
   toSend = ((Array == zeroVec) || (nextArray == Array)) == true ? 0 : 1;
	cout << "toSend " << toSend <<endl;
 }

/*inline void calculation(std::vector<std::vector<int> >& Array, std::vector<std::vector<int> >& nextArray)
{
  //gia ta esoterika
  int neighAlive;
  for(unsigned i =0; i < Array.size() ; ++i){
    for(unsigned j =0; j < Array[0].size() ; ++j){

      if(i == 0){
        if(j!=0 && j !=Array[0].size()-1 ){
                  neighAlive= (Array[Array.size()-1][j-1] == 1)? 1: 0
                  + (Array[Array.size()-1][j] == 1)? 1: 0
                  + (Array[Array.size()-1][j+1] == 1)? 1: 0
                  + (Array[0][j-1] == 1)? 1: 0
                  + (Array[0][j+1] == 1)? 1: 0
                  + (Array[i+1][j-1] == 1)? 1: 0
                  + (Array[i+1][j] == 1)? 1: 0
                  + (Array[i+1][j+1] == 1)? 1: 0;

                  nextArray[0][j] = ( neighAlive == 2 | 3 )? 1:0;
        }
        if(j == 0){
          neighAlive= (Array[Array.size()-1][Array[0].size() -1] == 1)? 1: 0
          + (Array[Array.size()-1][j] == 1)? 1: 0
          + (Array[Array.size()-1][j+1] == 1)? 1: 0
          + (Array[0][Array[0].size() -1] == 1)? 1: 0
          +(Array[0][j+1] == 1)? 1: 0
          + (Array[i+1][Array[0].size() -1] == 1)? 1: 0
          + (Array[i+1][j] == 1)? 1: 0
          + (Array[i+1][j+1] == 1)? 1: 0;

          nextArray[0][j] = ( neighAlive == 2 | 3 )? 1:0;
        }
        if(j == Array[0].size()-1){
          neighAlive= (Array[Array.size()-1][j-1] == 1)? 1: 0
          + (Array[Array.size()-1][j] == 1)? 1: 0
          + (Array[Array.size()-1][0] == 1)? 1: 0
          + (Array[0][j-1] == 1)? 1: 0
          +(Array[0][0] == 1)? 1: 0
          + (Array[i+1][j-1] == 1)? 1: 0
          + (Array[i+1][j] == 1)? 1: 0
          + (Array[i+1][0] == 1)? 1: 0;

          nextArray[0][j] = ( neighAlive == 2 | 3 )? 1:0;
        }
      }
	    if((i != 0 && i !=Array.size()-1) && (j != 0 && j!=Array[0].size()-1) )
      {

        neighAlive= (Array[i-1][j-1] == 1)? 1: 0
        + (Array[i-1][j] == 1)? 1: 0
        + (Array[i-1][j+1] == 1)? 1: 0
        + (Array[i][j-1] == 1)? 1: 0
        +(Array[i][j+1] == 1)? 1: 0
        + (Array[i+1][j-1] == 1)? 1: 0
        + (Array[i+1][j] == 1)? 1: 0
        + (Array[i+1][j+1] == 1)? 1: 0;

        nextArray[i][j] = ( neighAlive == 2 | 3 )? 1:0;
      }

      if(i == Array.size()-1){

        if(j!=0 && j !=Array[0].size()-1 ){
                  neighAlive= (Array[i-1][j-1] == 1)? 1: 0
                  + (Array[i-1][j] == 1)? 1: 0
                  + (Array[i-1][j+1] == 1)? 1: 0
                  + (Array[0][j-1] == 1)? 1: 0
                  +(Array[0][j+1] == 1)? 1: 0
                  + (Array[i][j-1] == 1)? 1: 0
                  + (Array[i][j+1] == 1)? 1: 0;

                  nextArray[i][j] = ( neighAlive == 2 | 3 )? 1:0;
        }
        if(j == 0){
          neighAlive= (Array[i-1][Array[0].size() -1] == 1)? 1: 0
          + (Array[i-1][j] == 1)? 1: 0
          + (Array[i-1][j+1] == 1)? 1: 0
          + (Array[0][Array[0].size() -1] == 1)? 1: 0
          + (Array[0][j+1] == 1)? 1: 0
          + (Array[i][Array[0].size() -1] == 1)? 1: 0
          + (Array[0][j] == 1)? 1: 0;
          + (Array[i][j+1] == 1)? 1: 0;

          nextArray[0][j] = ( neighAlive == 2 | 3 )? 1:0;
        }
        if(j == Array[0].size()-1){
          neighAlive= (Array[i][j-1] == 1)? 1: 0
          + (Array[i][0] == 1)? 1: 0
          + (Array[0][j-1] == 1)? 1: 0
          + (Array[0][0] == 1)? 1: 0
          + (Array[0][j] == 1)? 1: 0
          + (Array[i-1][j-1] == 1)? 1: 0
          + (Array[i-1][j] == 1)? 1: 0
          + (Array[i-1][0] == 1)? 1: 0;

          nextArray[0][j] = ( neighAlive == 2 | 3 )? 1:0;
        }

      }

      if(j == 0 && i!= 0 && i != Array.size()-1) {
        neighAlive= (Array[i-1][j+1] == 1)? 1: 0
        + (Array[i-1][j] == 1)? 1: 0
        + (Array[i-1][Array[0].size()-1] == 1)? 1: 0
        + (Array[i][j+1] == 1)? 1: 0
        +(Array[i][Array[0].size()-1] == 1)? 1: 0
        + (Array[i+1][Array[0].size()-1] == 1)? 1: 0
        + (Array[i+1][j] == 1)? 1: 0
        + (Array[i+1][j+1] == 1)? 1: 0;

        nextArray[i][j] = ( neighAlive == 2 | 3 )? 1:0;

      }

      else if(j == Array[0].size()-1 && i!= 0 && i != Array.size()-1) {
        neighAlive= (Array[i-1][j-1] == 1)? 1: 0
        + (Array[i-1][j] == 1)? 1: 0
        + (Array[i-1][0] == 1)? 1: 0
        + (Array[i][j-1] == 1)? 1: 0
        +(Array[i][0] == 1)? 1: 0
        + (Array[i+1][0] == 1)? 1: 0
        + (Array[i+1][j] == 1)? 1: 0
        + (Array[i+1][j-1] == 1)? 1: 0;

        nextArray[i][j] = ( neighAlive == 2 | 3 )? 1:0;
      }
    }
  }


}*/

inline void calculation(std::vector<std::vector<int> >& Array, std::vector<std::vector<int> >& nextArray)
{
  int aliveNeigh = 0;
  int n = Array.size();
  int m = Array[0].size();

  for (unsigned i = 0; i < n; ++i)
  {
    for (unsigned j = 0; j < m; ++j)
    {

      for (unsigned in = 0; in < 3; ++in)
      {
        for (unsigned ik = 0; ik < 3; ++ik)
        {
          if (ik == 1 && in == 1) continue;
          aliveNeigh += Array[(i + in -1 + n)%n][(j + ik -1 + m)%m];
        }
      }
      if (Array[i][j] == 1)
      {
        if (aliveNeigh == 2 || aliveNeigh == 3) nextArray[i][j] = 1;
        else nextArray[i][j] = 1;
      }
      else 
      {
        if (aliveNeigh == 3) nextArray[i][j] = 1;
        else nextArray[i][j] = 0;
      }
      aliveNeigh = 0;
    }
  }
}

void createRandomGrid(int noofColumns, int noofRows)
{
    FILE *pFile;
    pFile = fopen("./RandomArr", "wb");
    if (pFile == NULL)
    {
        cerr << "Couldn't open the file" << endl;
        fflush(stdout);
    }
    srand(time(NULL));
    int num;
    char pchar[2];
    string numberstring;
    for (int i = 0; i < noofRows; i++)
    {
        for (int j = 0; j < noofColumns; j++)
        {
            num = rand() % 2;
            sprintf(pchar, "%d", num);
            fwrite(pchar, sizeof(char), 1, pFile);
        }
        fwrite("\n", sizeof(char), 1, pFile);
    }
    fclose(pFile);
}



void findDimensions(string filename, int& noofcols, int& noofrows)
{
    string STRING;
    ifstream myfile(filename.c_str());
    if (!myfile)
    {
        cerr << "Couldn't open the file" << endl;
        fflush(stdout);
    }
    getline(myfile, STRING);
    noofcols = STRING.length();
    noofrows = count(istreambuf_iterator<char>(myfile), istreambuf_iterator<char>(), '\n') + 1;

    myfile.close();
}


void readCmdArgs(int argc, char *argv[], string& fileName, int& noofRows,int& noofCols)
{

    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-rows"))
        {
            noofRows = atoi(argv[i + 1]);
        }
        else if (!strcmp(argv[i], "-cols"))
        {
            noofCols = atoi(argv[i + 1]);
        }
        else
        if (!strcmp(argv[i], "-f"))
        {
            fileName = argv[i + 1];
            findDimensions(fileName,noofCols,noofRows);
        }
        else if (!strcmp(argv[i], "-r"))
        {
            fileName = "RandomArr";
            createRandomGrid(noofCols, noofRows);
        }
    }
}

void fillArray(string fileName,std::vector<std::vector<int> >& Array, int rows, int cols){

  std::ifstream infile(fileName);

  vector<int> tempBuf;
  int count = 0;
  string line;
int val;
  for (int i = 0; i < rows; i++)
  {
      tempBuf.clear();
      getline(infile, line);

      for (int j = 0; j < line.length(); j++)
      {
          val = line.at(j) - '0' ;
		tempBuf.push_back(val);

          count++;
      }
		Array.push_back(tempBuf);

  }
  infile.close();
}

int main(int argc, char *argv[]) {
  std::vector<std::vector<int> > Array;
  string filename;
  int rows, cols;
  readCmdArgs(argc, argv, filename, rows, cols);
  fillArray(filename,Array, rows, cols);
  std::vector<std::vector<int> > nextArray = Array;

  auto clearAll = [&]() {

      Array = nextArray;
        };

  int term = 1;
  #if DEF_LOOPS
      for (int loops = 0, total = 0; loops++ < DEF_LOOPS; )
  #else
      for (int total = 0; total < term; )
  #endif
  {
    calculation(Array, nextArray);

    Terminate(Array, nextArray, term); //term = 0 exit
    printGenerations(Array, nextArray);

    clearAll();
  }

  return 0;
}
