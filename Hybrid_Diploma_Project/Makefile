# Compilers
MPILD=$(PREP) mpicxx
NVCC=$(PREP) nvcc

#Flags
CPPFLAGS=-std=c++0x -Wall
OPENMPFLAGS=-fopenmp
CUDAFLAGS=-I${CUDA_INSTALL_PATH}/include
MPICFLAGS=-I${MPI_HOME}/include
NVCCFLAG=-lcudart -O3 -Xcompiler -march=native
NVCCMPICCFLAGS=-ccbin=mpicxx
NVCCOMPFLAGS=-lgomp

ES_PUREMPI_EX_PUREMPI=-DPUREMPI_ESOTERIC__HPP -DPUREMPI_EXOTERIC__HPP
ES_PUREMPI_EX_OPMP=-DPUREMPI_ESOTERIC__HPP -DOPENMP_EXOTERIC__HPP
ES_CUDANORMAL_EX_MPI=-DCUDA_ESOTERIC__HPP -DPUREMPI_EXOTERIC__HPP -DNORMAL_CUDA_MPI=1 -DCALCULATIONS__HPP
ES_CUDAAWARE_EX_MPI=-DCUDA_ESOTERIC__HPP -DPUREMPI_EXOTERIC__HPP -DCUDA_MPI_AWARE=1 -DCALCULATIONS__HPP
ES_CUDANORMAL_EX_OPMP=-DCUDA_ESOTERIC__HPP -DOPENMP_EXOTERIC__HPP -DNORMAL_CUDA_MPI=1 -DCALCULATIONS__HPP
ES_CUDAAWARE_EX_OPMP=-DCUDA_ESOTERIC__HPP -DOPENMP_EXOTERIC__HPP -DCUDA_MPI_AWARE=1 -DCALCULATIONS__HPP


#Binaries's Description
BINDIR=../bin
HYBRID_GOL_CUDA_NORMAL_MPI=$(BINDIR)/gol_cuda_normal_mpi
HYBRID_GOL_CUDA_AWARE_MPI=$(BINDIR)/gol_cuda_aware_mpi
HYBRID_GOL_CUDA_NORMAL_MPI_OPENMP=$(BINDIR)/gol_cuda_normal_mpi_openMP
HYBRID_GOL_CUDA_AWARE_MPI_OPENMP=$(BINDIR)/gol_cuda_aware_mpi_openMP
HYBRID_GOL_MPI_OPENMP=$(BINDIR)/gol_mpi_openMP
GOL_PURE_MPI=$(BINDIR)/gol_pure_mpi

#mpi+cuda
CUDA_MPI=$(HYBRID_GOL_CUDA_NORMAL_MPI) $(HYBRID_GOL_CUDA_AWARE_MPI)
#mpi+cuda+openMP
CUDA_MPI_OPENMP=$(HYBRID_GOL_CUDA_NORMAL_MPI_OPENMP) $(HYBRID_GOL_CUDA_AWARE_MPI_OPENMP)

#all binaries
BINARIES=$(CUDA_MPI) $(HYBRID_GOL_MPI_OPENMP) $(GOL_PURE_MPI) $(CUDA_MPI_OPENMP) 

# Commands
all: $(BINARIES)

main1.o: ParallelMain.cpp Makefile
	$(MPILD) $(CPPFLAGS) $(ES_PUREMPI_EX_PUREMPI) -c ParallelMain.cpp -o main1.o

main2.o: ParallelMain.cpp Makefile
	$(MPILD) $(CPPFLAGS) $(ES_PUREMPI_EX_OPMP) -fopenmp -c ParallelMain.cpp -o main2.o

main3.o: ParallelMain.cpp Makefile
	$(MPILD) $(CPPFLAGS) -DPUREMPI_EXOTERIC__HPP -DCUDA_ESOTERIC__HPP -DDEF_LOOPS=3 -c ParallelMain.cpp -o main3.o

main4.o: ParallelMain.cpp Makefile
	$(MPILD) $(CPPFLAGS) -DPUREMPI_EXOTERIC__HPP -DNORMAL_CUDA_MPI=1 -c ParallelMain.cpp -o main4.o

main5.o: ParallelMain.cpp Makefile
	$(MPILD) $(CPPFLAGS) -DPUREMPI_EXOTERIC__HPP -DNORMAL_CUDA_MPI=1 -c ParallelMain.cpp -o main5.o

main6.o: ParallelMain.cpp Makefile
	$(MPILD) $(CPPFLAGS) -DPUREMPI_EXOTERIC__HPP -DNORMAL_CUDA_MPI=1 -c ParallelMain.cpp -o main6.o

block.o: Block.cpp Block.h Makefile
	$(MPILD) $(CPPFLAGS) -c Block.cpp -o block.o

initialOperations.o: initialOperations.cpp initialOperations.h Arguments.h Makefile
	$(MPILD) $(CPPFLAGS) -c initialOperations.cpp -o initialOperations.o

neighProcesses.o: NeighProcesses.cpp NeighProcesses.hpp Block.h Makefile
	$(MPILD) $(CPPFLAGS) -c NeighProcesses.cpp -o neighProcesses.o

cudaEsoteric.o: cudaEsoteric.cu hybridCudaMPI.hpp Block.h Makefile
	$(NVCC) $(NVCCFLAGS) $(NVCCMPICCFLAGS) -c cudaEsoteric.cu -o cudaEsoteric.o #-lcudart -lcuda -lcublas

$(HYBRID_GOL_CUDA_NORMAL_MPI): main3.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o Makefile
	mkdir -p $(BINDIR)
	$(NVCC) $(MPICFLAGS) $(CUDAFLAGS) $(NVCCMPICCFLAGS) -DCUDA_ESOTERIC__HPP -o $(HYBRID_GOL_CUDA_NORMAL_MPI) main3.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o $(CUDAFLAGS) #-lcudart -lcuda -lcublas

$(HYBRID_GOL_CUDA_AWARE_MPI): main4.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o Makefile
	mkdir -p $(BINDIR)
	$(NVCC) $(MPICFLAGS) $(CUDAFLAGS) $(NVCCMPICCFLAGS) -o $(HYBRID_GOL_CUDA_AWARE_MPI) main4.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o $(CUDAFLAGS)

$(HYBRID_GOL_CUDA_NORMAL_MPI_OPENMP): main5.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o Makefile
	mkdir -p $(BINDIR)
	$(NVCC) $(MPICFLAGS) $(CUDAFLAGS) $(NVCCMPICCFLAGS) $(NVCCOMPFLAGS) -lgomp -o $(HYBRID_GOL_CUDA_NORMAL_MPI_OPENMP) main5.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o $(CUDAFLAGS)

$(HYBRID_GOL_CUDA_AWARE_MPI_OPENMP): main6.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o Makefile
	mkdir -p $(BINDIR)
	$(NVCC) $(MPICFLAGS) $(CUDAFLAGS) $(NVCCMPICCFLAGS) $(NVCCOMPFLAGS) -lgomp -o $(HYBRID_GOL_CUDA_AWARE_MPI_OPENMP) main6.o block.o initialOperations.o neighProcesses.o cudaEsoteric.o $(CUDAFLAGS)

$(HYBRID_GOL_MPI_OPENMP): main2.o block.o initialOperations.o neighProcesses.o Makefile
	mkdir -p $(BINDIR)
	$(NVCC) $(MPICFLAGS) $(CUDAFLAGS) $(NVCCMPICCFLAGS) $(NVCCOMPFLAGS) -lgomp -o $(HYBRID_GOL_MPI_OPENMP) main2.o block.o initialOperations.o neighProcesses.o

$(GOL_PURE_MPI): main1.o block.o initialOperations.o neighProcesses.o Makefile
	mkdir -p $(BINDIR)
	$(NVCC) $(MPICFLAGS) $(CUDAFLAGS) $(NVCCMPICCFLAGS) -o $(GOL_PURE_MPI) main1.o block.o initialOperations.o neighProcesses.o

clean:
	rm -rf *.o *~ $(BINARIES)
