/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file Block.cpp

    @brief  The implementation details of a process's block

    @notice Block implemented as a vector of vector of integers
    @notice There is the parallel reading and filling of each process Block
*/

#include <iostream>
#include "Block.h"

using namespace std;

/*
  @brief The constructor of the block

  @param[in] rows : The rows of the block
  @param[in] columns: The columns of the block
*/
Block::Block(int rows, int columns)
{

  this->bl.noofrows = rows;
  this->bl.noofcols = columns;
}

/*
  @brief This is the function of parallel reading of the given file and filling the Block of a Process

  @param[in]  rank: Process's Rank after Cartesian Topology
  @param[in]  coord[2]: Process's Coordinates in Topology
  @param[in]  dims[2]:  number of Processes Per Dimension (2D)
  @param[in]  filename: the name of the given file for parallel reading
  @param[in]  gridCols: the number of columns in the Grid
  @param[in]  gridRows: the number of rows in the Grid
  @param[in]  comm: MPI Communicator
  @param[in]  blockRows: the number of rows in each Block
  @param[in]  blockCols: the number of columns in each Block

*/
void Block::fillBlock(int rank, int coord[2], int dims[2], string filename, int gridCols, int gridRows, MPI_Comm comm, int blockRows, int blockCols)
{

  int x = coord[0];
  int y = coord[1];

  MPI_File in;
  int ierr;
  ierr = MPI_File_open(comm, filename.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &in); //anoigma arxeiou gia parallilo diavasma
  if (ierr)
  {
    EXIT();
  }
  int cellsPerBlockRow = this->bl.noofrows;
  int cellsPerBlockCol = this->bl.noofcols;
  /*Suntetagmenes se arxeio keimenou*/
  x = x * cellsPerBlockRow;
  y = y * cellsPerBlockCol;

  MPI_Datatype filetype;

  int bufferSize = cellsPerBlockRow * cellsPerBlockCol;
  char *buffer = new char[bufferSize * sizeof(int) + 1];
  int gsizes[2] = {gridRows, gridCols + 1};
  int lsizes[2] = {this->bl.noofrows, this->bl.noofcols};
  //int psizes[2] = {dims[0], dims[1]};
  //int coords[2] = {coord[0], coord[1]};
  int starts[2] = {x, y};

  MPI_Type_create_subarray(2, gsizes, lsizes, starts, MPI_ORDER_C, MPI_CHAR, &filetype);
  MPI_Type_commit(&filetype);
  MPI_File_set_view(in, 0, MPI_CHAR, filetype, "native", MPI_INFO_NULL);
  MPI_File_read_all(in, buffer, cellsPerBlockRow * cellsPerBlockCol, MPI_CHAR, MPI_STATUS_IGNORE);
  buffer[cellsPerBlockRow * cellsPerBlockCol] = '\0';
  vector<int> tempBuf;

  int count = 0;

  for (int i = 0; i < this->bl.noofrows; i++)
  {
    tempBuf.clear();
    for (int j = 0; j < this->bl.noofcols; j++)
    {
      int val = buffer[count] - '0';

      tempBuf.push_back(val);
      count++;
    }

    this->blockArray.push_back(tempBuf);
  }
}

/*
  @brief This is a host function for printing a single Row of a current Block

  @param[in] v: A Row of the Block
*/
void Block::printSingleRow(vector<int> v)
{
  for (auto i = v.begin(); i != v.end(); ++i)
    cout << "         " << *i << ' ';
  cout << endl;
}

/*
  @brief This is a host function for printing a Process's current Block

*/
void Block::printBlock()
{
  cout << this->blockArray.size() << endl;
  for (auto i = this->blockArray.begin(); i != this->blockArray.end(); ++i)
  {
    printSingleRow(*i);
    cout << endl;
  }
}

/*
  @brief This is a host function returning a specific row of the Block

  @param[in] rowNumber: The number of the row is chosen to be returned
*/
vector<int> &Block::getARow(int rowNumber)
{

  return this->blockArray[rowNumber];
}

/*
  @brief This is a host function returning a specific column of the Block

  @param[in] colNumber: The number of the column is chosen to be returned
  @param[in] col: The column - result of the function 
*/
void Block::getACol(int colNumber, vector<int> &col)
{
  int colj = 0;
  for (auto i = this->blockArray.begin(); i != this->blockArray.end(); ++i)
  {
    col.push_back(i[0][colNumber]);
    ++colj;
  }
}

/*
  @brief This is a host function for returning a specific element of the Block

  @param[in] i: The row number of the element
  @param[in] j: The column number of the element
*/
int Block::getSpecificIndex(int i, int j)
{

  return (this->blockArray[i][j]);
}
