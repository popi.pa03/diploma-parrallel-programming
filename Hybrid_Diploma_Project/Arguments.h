/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file Arguments.h

    @brief  The header containing the helper class Arguments used for better handling command-line Arguments 

*/
#ifndef ARGUMENTS__H
#define ARGUMENTS__H

#include <string>

using namespace std;

class Arguments
{

public:
  int noofColumns;
  int noofRows;
  char fileOrRandom;
  string filename;
  string operation;

  Arguments() {}
  //Arguments(int c, int r, string ForR): noofColumns (c), noofRows (r) {this->FileOrRandom = ForR;}

  void setMembers(int c, int r, char ForR, string FileName, string operation)
  {
    this->noofColumns = c;
    this->noofRows = r;
    this->fileOrRandom = ForR;
    this->filename = FileName;
    this->operation = operation;
  }

  void updateMembers(int noofcols, int noofrows)
  {
    this->noofColumns = noofcols;
    this->noofRows = noofrows;
  }
};

#endif
