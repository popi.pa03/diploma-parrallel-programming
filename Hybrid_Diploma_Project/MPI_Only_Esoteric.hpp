/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file MPI_Only_Esoteric.hpp

    @brief  The implementation details of pure MPI esoteric Calculation
*/
#include <iostream>
#include <vector>
#include "Block.h"

/*
	@brief Host function for calculation of esoteric items for next generation production.
	
	@param[in]	blockArrayNext: Block for storing the next Generation
	@param[in]	BL:	Block for current Generation

	@notice  ONLY MPI is used
*/
inline void pMPI_esotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &BL)
{

    int rows = BL.bl.noofrows;
    int cols = BL.bl.noofcols;

    int aliveNeighbours;

    for (int i = 1; i < rows - 1; i++)
    {
        aliveNeighbours = 0;
        for (int j = 1; j < cols - 1; j++)
        {
            if (BL.blockArray[i][j] == 1)
            {
                aliveNeighbours--; //it counts the block we are looking for too :(
                for (int neighbourCol = i - 1; neighbourCol < i + 2; neighbourCol++)
                {
                    for (int neighbourRow = j - 1; neighbourRow < j + 2; neighbourRow++)
                    {
                        if (BL.blockArray[neighbourCol][neighbourRow] == 1)
                        {
                            aliveNeighbours++;
                        }
                    }
                }
                if ((aliveNeighbours == 2) || (aliveNeighbours) == 3)
                {
                    blockArrayNext[i][j] = 1;
                }
                else
                {
                    blockArrayNext[i][j] = 0;
                }
                aliveNeighbours = 0;
            }
            else
            {
                for (int neighbourCol = i - 1; neighbourCol < i + 2; neighbourCol++)
                {
                    for (int neighbourRow = j - 1; neighbourRow < j + 2; neighbourRow++)
                    {
                        if (BL.blockArray[neighbourCol][neighbourRow] == 1)
                        {
                            aliveNeighbours++;
                        }
                    }
                }
                if (aliveNeighbours == 3)
                {
                    blockArrayNext[i][j] = 1;
                }

                aliveNeighbours = 0;
            }
        }
    }
}