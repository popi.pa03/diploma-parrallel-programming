/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

  @file   ParallelMain.cpp
  @brief  This contains the application entry point

  @notice Cmd Execution: ./exe -rows <nooRows> -cols <noofCols> {-f File, -r}

*/

#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <fstream>

#include <mpi.h>
#include "Block.h"
#include "initialOperations.h"
#include "NeighProcesses.hpp"

#include "Calculations.hpp"

#if CUDA_MPI_AWARE == 1 || NORMAL_CUDA_MPI == 1
#include "linkingCudaFunctions.hpp"
#endif

using namespace std;

#define DimsCheck()                                                                         \
    if (prodof(2, dims) != nprocs)                                                          \
    {                                                                                       \
        cout << "Dims_create returned the wrong decomposition. " << endl;                   \
        cout << "Is" << dims[0] << " x " << dims[1] << " should be 1 x " << nprocs << endl; \
        EXIT();                                                                             \
    }

/*
  @brief host function for printing process initial values

  @param[in]  rank : Process's rank Before Cartesian Topology
  @param[in]  rankAfterCartCreate: Process's rank After Cartesian Topology
  @param[in]  dims: number of Processes Per Dimension (2D)
  @param[in]  nprocs: number of Processes
  @param[in]  fileOrRandom: type of grid's data
  @param[in]  filename: Grid's data file: {Random Data-> RandomArr or fileName of an existing file}
  @param[in]  noofcols: Grid's columns
  @param[in]  noofrows: Grid's rows

*/
void printEssentialProcessValues(int rank, int rankAfterCartCreate, int dims[2], int nprocs, char fileOrRandom, string filename, int noofcols, int noofrows)
{

    cout << " ************************** " << endl;
    cout << " << PROCESS"
         << " rank before Topology :: " << rank << " rank after Topology ::" << rankAfterCartCreate << " >> " << endl;
    cout << " __________________________________________________________________________ " << endl;
    /*  cout << endl
         << "    COMMON VALUES  (All Processes have): " << endl
         << endl;

    cout << "       DataSource: " << endl
         << "          grid's dimensions: " << noofcols << " * " << noofrows << endl;
    cout << "         Filename: " << filename << endl;

    cout << "       Topology: number of ALL Processes:  " << nprocs << endl;
    cout << "       Topology: number of Processes Per Dimension: " << dims[0] << " * " << dims[1] << endl;*/
}

/*
  @brief Host function for printing the Block of the Next Generation

  @param[in]  blockArrayNext: block of the next generation

*/
void printNewGen(vector<vector<int>> &blockArrayNext)
{

    cout << "NEXT GENERATION:::::: " << endl;

    for (auto i = blockArrayNext.begin(); i != blockArrayNext.end(); ++i)
    {
        for (auto j = i->begin(); j != i->end(); ++j)
        {
            cout << "         " << *j << ' ';
        }
        cout << endl;
    }
}

/*
  @brief Host function for printing process's Values

  @param[in]  nprocs: number of processes
  @param[in]  rank: process's rank before cartesian topology 
  @param[in]  rankAfterCartCreate: process's rank after cartesian topology
  @param[in]  dims[2]: number of Processes Per Dimension (2D)
  @param[in]  Args: Arguments from command line
  @param[in]  currentGeneration: Block of Current Generation
  @param[in]  NPRs: Neighbour Processes Ranks
  @param[in]  blockArrayNext : Block of Next Generation
  @param[in]  termCheck: termination Check result of current Generation

*/
void printProcessesValues(int nprocs, int rank, int rankAfterCartCreate, int dims[2], Arguments *Args, Block *currentGeneration, NeighProcesses *NPRs, vector<vector<int>> blockArrayNext, bool termCheck)
{

    for (int p = 0; p < nprocs; p++)
    {
        if (rank == p)
        {
            printEssentialProcessValues(rank, rankAfterCartCreate, dims, nprocs, Args->fileOrRandom, Args->filename, Args->noofColumns, Args->noofRows);
            cout << endl
                 << endl
                 << "     PROCESSES BLOCK is " << endl;

            currentGeneration->printBlock();

            cout << endl;
            /*                 << "         NeighBour Process Ranks <up , down, left , right, upL, upR downL, downR > " << NPRs->destinationup << " " << NPRs->destinationdown << " " << NPRs->destinationleft << " " << NPRs->destinationright
                 << " " << NPRs->upLeftProcessRank << " " << NPRs->upRightProcessRank
                 << " " << NPRs->downLeftProcessRank << " " << NPRs->downRightProcessRank << endl;

            cout << " NeighBour Lines:  " << endl; //NPRs-> lastRowofUpperNeigh << " " << NPRs->firstRowofDownNeigh << endl; //" " << NPRs->lastColumnofLeftNeigh << " " << NPRs->firstColumnofRightNeigh << endl;

            cout << "last row of upper Neigh: " << endl;
            for (auto i = NPRs->rcvdFromUpperNeigh.begin(); i != NPRs->rcvdFromUpperNeigh.end(); ++i)
                cout << "         " << *i << ' ';
            cout << endl;
            cout << "first row of down Neigh: " << endl;
            for (auto i = NPRs->rcvdFromDownNeigh.begin(); i != NPRs->rcvdFromDownNeigh.end(); ++i)
                cout << "         " << *i << ' ';
            cout << endl;
            cout << "FROM LEFT NEIGH: " << endl;
            for (auto i = NPRs->rcvdFromLeftNeigh.begin(); i != NPRs->rcvdFromLeftNeigh.end(); ++i)
                cout << "         " << *i << ' ';
            cout << endl;
            cout << "FROM right NEIGH " << endl;
            for (auto i = NPRs->rcvdFromRightNeigh.begin(); i != NPRs->rcvdFromRightNeigh.end(); ++i)
                cout << "         " << *i << ' ';
            cout << endl;

            cout << "SINGLE VALS: " << endl
                 << "downRight: " << NPRs->downRightSingle << " downLeft: " << NPRs->downLeftSingle << " upRight: " << NPRs->upRightSingle << " upLeft: " << NPRs->upLeftSingle << endl;

            cout << endl
	    << endl;*/
            printNewGen(blockArrayNext);

            cout << endl;
            // << " termCheck::" << termCheck << endl;*/
        }

        MPI_Barrier(MPI_COMM_WORLD);
    }
}

/*
  @brief Host function for clearing the vectors of received values, so as to compute the next generation

  @param[in]  NPRs: Object contains the vectors of the neighbour's received values 

*/
void clearReceivedValues(NeighProcesses &NPRs)
{
    NPRs.rcvdFromDownNeigh.clear();
    NPRs.rcvdFromUpperNeigh.clear();
    NPRs.rcvdFromRightNeigh.clear();
    NPRs.rcvdFromLeftNeigh.clear();
}

/**
  @brief The application entry point
 
  @param[in] argc	The number of command-line arguments
  @param[in] argv	The list of command-line arguments
 */
int main(int argc, char *argv[])
{

    Arguments Args;
    int period[2] = {1, 1}, dims[2] = {0, 0}, coords[2];
    int rankAfterCartCreate, nprocs, rank, reorder = 1;
    MPI_Comm comm;

//this will affect only for MPI_Aware
#if CUDA_MPI_AWARE
    SetDeviceBeforeInit();
#endif

    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    getInitialValues(rank, argc, nprocs, argv, Args);

    MPI_Dims_create(nprocs, 2, dims);
    DimsCheck();

    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, period, reorder, &comm);

    MPI_Cart_coords(comm, rank, 2, coords);

    MPI_Cart_rank(comm, coords, &rankAfterCartCreate);

// Setting the device here will have effect only for the normal CUDA & MPI version
#if NORMAL_CUDA_MPI
    SetDeviceAfterInit(rank);
#endif
    int blockRows = Args.noofRows / dims[0];
    int blockCols = Args.noofColumns / dims[1];

    Block currentGeneration = Block(blockRows, blockCols);

    currentGeneration.fillBlock(rankAfterCartCreate, coords, dims, Args.filename, Args.noofColumns, Args.noofRows, comm, blockRows, blockCols);

    NeighProcesses NPRs = NPRs.findNeighbourProcesses(coords, comm);

    vector<vector<int>> blockArrayNext = currentGeneration.blockArray;

    int toSend = 0;

    auto clearAll = [&]() {
        currentGeneration.blockArray = blockArrayNext;

        clearReceivedValues(NPRs);
    };

    double counter = 0.0;
    counter -= MPI_Wtime();
    int loops = 0;
#if DEF_LOOPS
    for (int loops = 0, total = 0; total < nprocs && loops++ < DEF_LOOPS; MPI_Allreduce(&toSend, &total, 1, MPI_INT, MPI_SUM, comm))
#else
    for (int total = 0; total < nprocs; MPI_Allreduce(&toSend, &total, 1, MPI_INT, MPI_SUM, comm))
#endif
    {

        NPRs.sendReceiveCall(rankAfterCartCreate, blockCols, blockRows, &currentGeneration, comm, &NPRs);

        exotericCalculation(blockArrayNext, currentGeneration, NPRs);

        esotericCalculation(blockArrayNext, currentGeneration);

        Terminate(blockArrayNext, currentGeneration.blockArray, toSend);

        //printProcessesValues(nprocs, rank, rankAfterCartCreate, dims, &Args, &currentGeneration, &NPRs, blockArrayNext, 1);
        clearAll();
        loops++;
    }

    printProcessesValues(nprocs, rank, rankAfterCartCreate, dims, &Args, &currentGeneration, &NPRs, blockArrayNext, loops);

    counter += MPI_Wtime();
    double maxElapsedTime = 0.0;
    MPI_Reduce(&counter, &maxElapsedTime, 1, MPI_DOUBLE, MPI_MAX, 0, comm);

    if (rankAfterCartCreate == 0)
        cout << "maxElapsedTime:: " << maxElapsedTime << endl;

    MPI_Finalize();

    return 0;
}
