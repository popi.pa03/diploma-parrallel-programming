#ifndef BLOCK__H
#define BLOCK__H
#include <vector>
#include <cstring>

#include <fstream>

#include "mpi.h"

#define EXIT()      \
    fflush(stdout); \
    MPI_Abort(MPI_COMM_WORLD, 1);

typedef struct block_dimensions
{
    int noofrows;
    int noofcols;
} block_dimensions;

class Block
{

  public:
    std::vector<std::vector<int>> blockArray;
    struct block_dimensions bl;

    Block(int rows, int columns);

    void fillBlock(int rank, int coord[2], int dims[2], std::string filename, int gridCols, int gridRows, MPI_Comm comm, int blockRows, int blockCols);

    std::vector<int> &getARow(int rowNumber);
    void getACol(int colNumber, std::vector<int> &col);
    int getSpecificIndex(int i, int j);

    void printSingleRow(std::vector<int>);
    void printBlock();
};

#endif
