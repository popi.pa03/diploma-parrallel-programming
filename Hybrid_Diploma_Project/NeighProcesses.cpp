/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

  @file   NeighProcesses.cpp
  @brief  This contains the host functions for identifying the neighbour process's ranks and values
*/
#include <iostream>
#include "NeighProcesses.hpp"

/*
  @brief Host function identifying neighbour processes ranks

  @param[in]  coords : Topology coordinates
  @param[in]  comm: MPI Communicator

*/
NeighProcesses NeighProcesses::findNeighbourProcesses(int *coords, MPI_Comm comm)
{

  int downLeftProcessRank, upRightProcessRank, upLeftProcessRank, downRightProcessRank;
  int destinationup, destinationdown, destinationleft, destinationright;

  MPI_Cart_shift(comm, 0, 1, &destinationup, &destinationdown);    //up - down
  MPI_Cart_shift(comm, 1, 1, &destinationleft, &destinationright); //right - left

  int newCoords[2];
  //Upright
  newCoords[0] = coords[0] - 1;
  newCoords[1] = coords[1] + 1;
  MPI_Cart_rank(comm, newCoords, &upRightProcessRank);

  //UpLeft
  newCoords[0] = coords[0] - 1;
  newCoords[1] = coords[1] - 1;
  MPI_Cart_rank(comm, newCoords, &upLeftProcessRank);

  //DownRight
  newCoords[0] = coords[0] + 1;
  newCoords[1] = coords[1] - 1;
  MPI_Cart_rank(comm, newCoords, &downRightProcessRank);

  //DownLeft
  newCoords[0] = coords[0] + 1;
  newCoords[1] = coords[1] + 1;
  MPI_Cart_rank(comm, newCoords, &downLeftProcessRank);

  NeighProcesses NPRs = NeighProcesses(destinationup, destinationdown, destinationleft, destinationright, downLeftProcessRank, upRightProcessRank, upLeftProcessRank, downRightProcessRank);

  return NPRs;
}

/*
  @brief Host function for sending and receiving non blocking neighbour processes values
  
  @param[in] rank: process's rank after Cartesian topology
  @param[in] noofcols: process's block columns
  @param[in] noofrows: process's block rows
  @param[in] currentGeneration: process's current block generation
  @param[in] comm: Mpi communicator
  @param[in] NPRs: Helper Object for Neighbour Processes ranks and received values

  @notice Rows and Columns are sent and received as special datatypes
*/
void NeighProcesses::sendAndReceiveNeighBourValues(int rank, int noofcols, int noofrows, Block *currentGeneration, MPI_Comm comm, NeighProcesses *NPRs)
{

  // process_boundary_and_pack<<<gs_b,bs_b,0,s1>>>(u_new_d,u_d,to_left_d,to_right_d,n,m);
  // process_inner_domain<<<gs_id,bs_id,0,s2>>>(u_new_d, u_d,to_left_d,to_right_d,n,m);
  // cudaStreamSynchronize(s1); //wait for boundary

  int downLeftProcessRank = NPRs->downLeftProcessRank, upRightProcessRank = NPRs->upRightProcessRank, upLeftProcessRank = NPRs->upLeftProcessRank, downRightProcessRank = NPRs->downRightProcessRank;
  int destinationup = NPRs->destinationup, destinationdown = NPRs->destinationdown, destinationleft = NPRs->destinationleft, destinationright = NPRs->destinationright;

  int *frstRow = new int[noofcols], *lastRow = new int[noofcols];

  MPI_Request allRequestsToWait[8];
  MPI_Request allRequestsToStart[8];
  MPI_Status allStatuses[8];

  MPI_Request request0, request1, request2, request3, request4, request5, request6, request7;
  MPI_Request rrequest0, rrequest1, rrequest2, rrequest3, rrequest4, rrequest5, rrequest6, rrequest7;
  MPI_Request request00, request10, request20, request30, request40, request50, request60, request70;

  /*Dimiourgia datatypes gia tin apostoli grammwn ki stilwn*/
  MPI_Datatype Row;
  MPI_Type_contiguous(noofcols, MPI_INT, &Row);
  MPI_Type_commit(&Row);

  MPI_Datatype Col;
  MPI_Type_contiguous(noofrows, MPI_INT, &Col);
  MPI_Type_commit(&Col);

  std::vector<int> column0;
  int *columnArr0 = new int[noofrows * sizeof(int)];
  currentGeneration->getACol(0, column0);

  for (unsigned int i = 0; i < column0.size(); i++)
  {
    columnArr0[i] = column0[i];
  }

  std::vector<int> columnN;
  int *columnArrN = new int[noofrows * sizeof(int)];
  currentGeneration->getACol(noofcols - 1, columnN);
  for (unsigned int i = 0; i < columnN.size(); i++)
  {
    columnArrN[i] = columnN[i];
  }

  int *rcvdCol0 = new int[noofrows * sizeof(int)];
  int *rcvdCol1 = new int[noofrows * sizeof(int)];

  //ROWS
  MPI_Send_init(&(currentGeneration->blockArray[0][0]), 1, Row, destinationup, rank, comm, &request0);
  MPI_Send_init(&(currentGeneration->blockArray[noofrows - 1][0]), 1, Row, destinationdown, rank, comm, &request1);

  //COLS
  MPI_Send_init(columnArr0, 1, Col, destinationleft, rank, comm, &request2);
  MPI_Send_init(columnArrN, 1, Col, destinationright, rank, comm, &request3);

  //SINGLE ANGLE CELLS
  MPI_Send_init(&(currentGeneration->blockArray[0][0]), 1, MPI_INT, upLeftProcessRank, rank, comm, &request4);                                                                      //panw aristera
  MPI_Send_init(&(currentGeneration->blockArray[currentGeneration->bl.noofrows - 1][0]), 1, MPI_INT, downLeftProcessRank, rank, comm, &request5);                                   //katw aristera
  MPI_Send_init(&(currentGeneration->blockArray[0][currentGeneration->bl.noofcols - 1]), 1, MPI_INT, upRightProcessRank, rank, comm, &request6);                                    //panw deksia
  MPI_Send_init(&(currentGeneration->blockArray[currentGeneration->bl.noofrows - 1][currentGeneration->bl.noofcols - 1]), 1, MPI_INT, downRightProcessRank, rank, comm, &request7); //katw deksia

  //ROWS
  MPI_Irecv(frstRow, 1, Row, destinationdown, destinationdown, comm, &rrequest0);
  MPI_Irecv(lastRow, 1, Row, destinationup, destinationup, comm, &rrequest1);

  //COLS
  MPI_Irecv(rcvdCol0, noofrows, MPI_INT, destinationright, destinationright, comm, &rrequest2);
  MPI_Irecv(rcvdCol1, noofrows, MPI_INT, destinationleft, destinationleft, comm, &rrequest3);

  //SINGLE ANGLE CELLS
  int downright, downleft, upright, upleft;
  MPI_Irecv(&upleft, 1, MPI_INT, upLeftProcessRank, upLeftProcessRank, comm, &rrequest4);
  MPI_Irecv(&upright, 1, MPI_INT, upRightProcessRank, upRightProcessRank, comm, &rrequest5);
  MPI_Irecv(&downleft, 1, MPI_INT, downLeftProcessRank, downLeftProcessRank, comm, &rrequest6);
  MPI_Irecv(&downright, 1, MPI_INT, downRightProcessRank, downRightProcessRank, comm, &rrequest7);

  //ROWS
  MPI_Recv_init(&(currentGeneration->blockArray[0][0]), 1, Row, destinationup, rank, comm, &request00);
  MPI_Recv_init(&(currentGeneration->blockArray[noofrows - 1][0]), 1, Row, destinationdown, rank, comm, &request10);

  //COLS
  MPI_Recv_init(columnArr0, 1, Col, destinationleft, rank, comm, &request20);
  MPI_Recv_init(columnArrN, 1, Col, destinationright, rank, comm, &request30);

  //SINGLE ANGLE CELLS
  MPI_Recv_init(&(currentGeneration->blockArray[0][0]), 1, MPI_INT, upLeftProcessRank, rank, comm, &request40);
  MPI_Recv_init(&(currentGeneration->blockArray[currentGeneration->bl.noofrows - 1][0]), 1, MPI_INT, downLeftProcessRank, rank, comm, &request50);
  MPI_Recv_init(&(currentGeneration->blockArray[0][currentGeneration->bl.noofcols - 1]), 1, MPI_INT, upRightProcessRank, rank, comm, &request60);
  MPI_Recv_init(&(currentGeneration->blockArray[currentGeneration->bl.noofrows - 1][currentGeneration->bl.noofcols - 1]), 1, MPI_INT, downRightProcessRank, rank, comm, &request70);

  //ROWS
  MPI_Isend(&(currentGeneration->blockArray[0][0]), 1, Row, destinationup, rank, comm, &request00);
  MPI_Isend(&(currentGeneration->blockArray[noofrows - 1][0]), 1, Row, destinationdown, rank, comm, &request10);

  //COLS
  MPI_Isend(columnArr0, 1, Col, destinationleft, rank, comm, &request20);
  MPI_Isend(columnArrN, 1, Col, destinationright, rank, comm, &request30);

  //SINGLE ANGLE CELLS
  MPI_Isend(&(currentGeneration->blockArray[0][0]), 1, MPI_INT, upLeftProcessRank, rank, comm, &request40);
  MPI_Isend(&(currentGeneration->blockArray[currentGeneration->bl.noofrows - 1][0]), 1, MPI_INT, downLeftProcessRank, rank, comm, &request50);
  MPI_Isend(&(currentGeneration->blockArray[0][currentGeneration->bl.noofcols - 1]), 1, MPI_INT, upRightProcessRank, rank, comm, &request60);
  MPI_Isend(&(currentGeneration->blockArray[currentGeneration->bl.noofrows - 1][currentGeneration->bl.noofcols - 1]), 1, MPI_INT, downRightProcessRank, rank, comm, &request70);

  allRequestsToWait[0] = rrequest0;
  allRequestsToWait[1] = rrequest1;
  allRequestsToWait[2] = rrequest2;
  allRequestsToWait[3] = rrequest3;
  allRequestsToWait[4] = rrequest4;
  allRequestsToWait[5] = rrequest5;
  allRequestsToWait[6] = rrequest6;
  allRequestsToWait[7] = rrequest7;

  allRequestsToStart[0] = request0;
  allRequestsToStart[1] = request1;
  allRequestsToStart[2] = request2;
  allRequestsToStart[3] = request3;
  allRequestsToStart[5] = request5;
  allRequestsToStart[6] = request6;
  allRequestsToStart[4] = request4;
  allRequestsToStart[7] = request7;

  MPI_Startall(8, allRequestsToStart);
  MPI_Waitall(8, allRequestsToWait, allStatuses);

  // unpack<<<gs_s,bs_s>>>(u_new_d, from_left_d, from_right_d, n, m);
  // cudaDeviceSynchronize(); //wait for iteration to finish

  MPI_Request_free(&request0);
  MPI_Request_free(&request1);
  MPI_Request_free(&request2);
  MPI_Request_free(&request3);
  MPI_Request_free(&request4);
  MPI_Request_free(&request5);
  MPI_Request_free(&request6);
  MPI_Request_free(&request7);
  MPI_Request_free(&request00);
  MPI_Request_free(&request10);
  MPI_Request_free(&request20);
  MPI_Request_free(&request30);
  MPI_Request_free(&request40);
  MPI_Request_free(&request50);
  MPI_Request_free(&request60);
  MPI_Request_free(&request70);

  for (int i = 0; i < noofcols; i++)
  {
    NPRs->rcvdFromUpperNeigh.push_back(lastRow[i]);
  }

  for (int i = 0; i < noofcols; i++)
  {
    NPRs->rcvdFromDownNeigh.push_back(frstRow[i]);
  }

  for (int i = 0; i < noofrows; i++)
  {
    NPRs->rcvdFromLeftNeigh.push_back(rcvdCol1[i]);
  }

  for (int i = 0; i < noofrows; i++)
  {
    NPRs->rcvdFromRightNeigh.push_back(rcvdCol0[i]);
  }

  NPRs->upLeftSingle = upleft;
  NPRs->upRightSingle = upright;
  NPRs->downLeftSingle = downleft;
  NPRs->downRightSingle = downright;

  delete frstRow;
  delete lastRow;
  delete rcvdCol0;
  delete rcvdCol1;
  delete columnArr0;
  delete columnArrN;

  MPI_Type_free(&Row);
  MPI_Type_free(&Col);
}
