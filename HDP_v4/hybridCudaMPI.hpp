/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file hybridCudaMPI.hpp
    @brief The header containing host functions for Esoteric Calculations using Cuda

*/
#ifndef HYBRIDCUDAMPI__HPP
#define HYBRIDCUDAMPI__HPP

#include <cuda_runtime.h>
#include <vector>
#define CUDA_CALL(F)                                                          \
    if ((F) != cudaSuccess)                                                   \
    {                                                                         \
        printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
               __FILE__, __LINE__);                                           \
        exit(-1);                                                             \
    }

/*
  This is the environment variable which allows the reading of the local rank of the current MPI
  process before the MPI environment gets initialized with MPI_Init(). This is necessary when running
  the CUDA-aware MPI version, which needs this information in order to be able to
  set the CUDA device for the MPI process before MPI environment initialization. If you are using MVAPICH2, 
  set this constant to "MV2_COMM_WORLD_LOCAL_RANK"; for Open MPI, use "OMPI_COMM_WORLD_LOCAL_RANK".  
 
 MORE AT: https://its.northeastern.edu/researchcomputing/wp-content/uploads/2018/07/Parallel_Computing-Seminar_NEU-RC-2014_12_16.pdf
 */
#define ENV_LOCAL_RANK "MV2_COMM_WORLD_LOCAL_RANK"

#ifdef __cplusplus
extern "C"
{
#endif

    void gameOfLifeGPU(int **h_prevBlock, int **h_currentBlock, int nRows, int nColumns);
    void vecToArr(std::vector<std::vector<int>> &vals, int **&newArr);
    void arrToVec(std::vector<std::vector<int>> &vals, int **&newArr);
    void cuda_esotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &BL);

#ifdef __cplusplus
}
#endif

#endif
