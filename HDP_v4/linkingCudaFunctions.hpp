/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

  @file   linkingCudaFunctions.h
  @brief  Header helper file for better linking cuda and mpi file.
  
  @notice NVCC does not support c++14

*/
#ifndef LINKINGCUDAFUNCTIONS__HPP
#define LINKINGCUDAFUNCTIONS__HPP

#include "hybridCudaMPI.hpp"

/*
    @brief  Host Function that allows the MPI process to set the CUDA device before the MPI environment is initialized.
            This is for both Cuda-aware MPI version and Normal one.
            For the Normal Version (without Cuda awareness) this will not do anything.
            For the CUDA-aware MPI version, the is the only place where the device gets set. 
            To achieve this, we use the node's local rank, as the MPI environment has not been initialized yet.
*/
inline void SetDeviceBeforeInit()
{
    char *localRankStr = NULL;
    int rank = 0, devCount = 0;

    // We extract the local rank initialization using an environment variable
    if ((localRankStr = getenv(ENV_LOCAL_RANK)) != NULL)
    {
        rank = atoi(localRankStr);
    }

    CUDA_CALL(cudaGetDeviceCount(&devCount));
    CUDA_CALL(cudaSetDevice(rank % devCount));
}

/*
    @brief  Host Function that allows the MPI process to set the CUDA device after the MPI environment is initialized.
            This is for both Cuda-aware MPI version and Normal one.
            For the Normal Version (without Cuda awareness) we use the process rank as the MPI is initialized
            For the CUDA-aware MPI version, this will not affect anything.

    @param[in]  rank: Process Rank after Cartesian Topology
 */
inline void SetDeviceAfterInit(int rank)
{
    int devCount = 0;

    CUDA_CALL(cudaGetDeviceCount(&devCount));
    CUDA_CALL(cudaSetDevice(rank % devCount));
}

/*
    NOT COMPLETED
    @brief  Host Function that writes received results to Cuda Memory. This works only for Normal-Cuda-MPI
    
*/
inline void ExchangeData(int *dest, int *src, int byteCount, int transferOption)
{
    byteCount = byteCount * sizeof(int);
    if (transferOption == 1)
    {
        CUDA_CALL(cudaMemcpy(dest, src, byteCount, cudaMemcpyHostToDevice));
    }
    else
    {
        CUDA_CALL(cudaMemcpy(src, dest, byteCount, cudaMemcpyDeviceToHost));
    }
}

#endif
