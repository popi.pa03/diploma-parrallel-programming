/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

  @file   NeighProcesses.h
  @brief  Header file for identifying the neighbour process's ranks and values
*/

#ifndef NEIGHPROCESSES__HPP
#define NEIGHPROCESSES__HPP

#include <vector>
#include "mpi.h"
#include "Block.h"

class NeighProcesses
{

  public:
    int destinationup, destinationdown, destinationleft, destinationright;
    int downLeftProcessRank, upRightProcessRank, upLeftProcessRank, downRightProcessRank;
    std::vector<int> rcvdFromDownNeigh, rcvdFromUpperNeigh, rcvdFromRightNeigh, rcvdFromLeftNeigh;
    int downRightSingle, downLeftSingle, upRightSingle, upLeftSingle;

    NeighProcesses(int destinationup, int destinationdown, int destinationleft, int destinationright, int downLeftProcessRank, int upRightProcessRank, int upLeftProcessRank, int downRightProcessRank)
    {
        this->destinationup = destinationup;
        this->destinationdown = destinationdown;
        this->destinationleft = destinationleft;
        this->destinationright = destinationright;
        this->downLeftProcessRank = downLeftProcessRank;
        this->downRightProcessRank = downRightProcessRank;
        this->upLeftProcessRank = upLeftProcessRank;
        this->upRightProcessRank = upRightProcessRank;
    }
    NeighProcesses findNeighbourProcesses(int *coords, MPI_Comm comm);

    void sendAndReceiveNeighBourValues(int rank, int noofcols, int noofrows, Block *currentGeneration, MPI_Comm comm, NeighProcesses *NPRs);

    inline void sendReceiveCall(int rank, int noofcols, int noofrows, Block *currentGeneration, MPI_Comm comm, NeighProcesses *NPRs)
    {
        this->sendAndReceiveNeighBourValues(rank, noofcols, noofrows, currentGeneration, comm, NPRs);
    }
};

#endif
