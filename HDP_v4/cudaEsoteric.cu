#include <stdio.h>
#include <cuda.h>
#include <math.h>

#include "Block.h"
#include "hybridCudaMPI.hpp"

#define CUDA_CALL(F)  if( (F) != cudaSuccess ) \
  {	printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
   __FILE__,__LINE__); exit(-1);}


texture<int, cudaTextureType2D, cudaReadModeElementType> texRef;

__global__ void updatedValueGPU(int* prevBlock, int* currentBlock, int nRows, int nColumns)
{
	const unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	int xCoord;
	int yCoord;
	int lowX, lowY, maxX, maxY;
	int activeNeigh;

	if (i < nRows*nColumns)
	{
		xCoord = i / nColumns;
		yCoord = i - xCoord * nColumns;

		lowX = nColumns * ((xCoord - 1 + nRows) % nRows); //the up element on x axis, when -1 goes to nRows-1
		lowY = (yCoord - 1 + nColumns) % nColumns; //the up element on y axis, when -1 goes to nColumns-1

		maxX = nColumns * ((xCoord + 1) % nRows); //the down element on x axis,  when nRows goes to 0
		maxY = (yCoord + 1) % nColumns; //the down element on y axis

		activeNeigh = prevBlock[lowX + lowY] + prevBlock[lowX + yCoord] + prevBlock[lowX + maxY] //the up 3 neighbors
			+ prevBlock[xCoord*nColumns + lowY] + prevBlock[xCoord*nColumns + maxY] //the middle neighbors
			+ prevBlock[maxX + lowY] + prevBlock[maxX + yCoord] + prevBlock[maxX + maxY]; //the down 3 neighbors


		if (activeNeigh == 3 || (activeNeigh == 2 && prevBlock[xCoord*nColumns + yCoord] == 1)) //find new value of the cell
		{
			currentBlock[xCoord*nColumns + yCoord] = tex2D(texRef,1,1);
		}
		else
		{
			currentBlock[xCoord*nColumns + yCoord] = tex2D(texRef, 0, 0);
		}
	}

}

extern "C" void vecToArr(std::vector<std::vector<int> > &vals, int**& newArr)
{
	for (unsigned i = 0; (i < vals.size()); i++)
	{
		for (unsigned j = 0; (j < vals[i].size()); j++)
		{
			newArr[i][j] = vals[i][j];
		}
	}
}

extern "C" void arrToVec(std::vector<std::vector<int> > &vals, int**& newArr)
{
	for (unsigned i = 1; (i < vals.size() - 2); i++)
	{
		for (unsigned j = 1; (j < vals[i].size() - 2); j++)
		{
			vals[i][j] = newArr[i][j];
		}
	}
}

extern "C" void gameOfLifeGPU(int** h_Block, int nRows, int nColumns)
{
	int memSize;
	const int BLOCK_SIZE = 256;
	int N = nRows * nColumns * sizeof(int);

	memSize = nRows * nColumns * sizeof(int);

	// Allocate CUDA array in device memory
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0,
		cudaChannelFormatKindUnsigned);

	cudaArray* cuArray;

	CUDA_CALL(cudaMallocArray(&cuArray, &channelDesc, nRows, nColumns));
	CUDA_CALL(cudaMemcpyToArray(cuArray, 0, 0, h_Block, memSize, cudaMemcpyHostToDevice));

	// Set texture parameters
	texRef.addressMode[0] = cudaAddressModeWrap;
	texRef.addressMode[1] = cudaAddressModeWrap;
	texRef.filterMode = cudaFilterModeLinear;
	texRef.normalized = true;

	// Bind the array to the texture reference
	cudaBindTextureToArray(texRef, cuArray, channelDesc);

	int* d_currentBlock;
	CUDA_CALL(cudaMalloc(&d_currentBlock, memSize));

	updatedValueGPU << <(N + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE >> > ((int*)cuArray, d_currentBlock, nColumns, nRows);


	CUDA_CALL(cudaMemcpy(h_Block, d_currentBlock, memSize, cudaMemcpyDeviceToHost));
	CUDA_CALL(cudaThreadSynchronize());

	CUDA_CALL(cudaFreeArray(cuArray));
	CUDA_CALL(cudaFree(d_currentBlock));

}

extern "C" void cuda_esotericCalculation(std::vector<std::vector<int> >& blockArrayNext, Block &BL)
{
	int ** h_prevBlock;

	h_prevBlock = new int*[BL.blockArray.size()];
	for (int i = 0; i < BL.blockArray.size(); i++) {
		h_prevBlock[i] = new int[BL.blockArray[i].size()];
	}

	vecToArr(BL.blockArray, h_prevBlock);

	gameOfLifeGPU(h_prevBlock, BL.blockArray.size(), BL.blockArray[0].size());

	arrToVec(blockArrayNext, h_prevBlock);


	delete[] h_prevBlock;
	h_prevBlock = NULL;
}
