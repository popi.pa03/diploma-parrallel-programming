/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file exotericCalculations.h
    @brief The header containing host functions for Exoteric Calculations(pure MPI and Hybrid mpi-openMP) &  Termination Check

*/
#include <vector>
#include "Block.h"
#include "NeighProcesses.h"

/*
	@brief Helper host Function for calculating the Angle Cells using pure MPI
	
	@param[in]	blockArrayNext : Block of the next Generation
	@param[in]	blockArrayCurrent : Block of the current Generation
	@param[in]	topLeft: rank of the top Left neighbour process.
	@param[in]	botLeft: rank of the bottom Left neighbour process.
	@param[in]	topRight: rank of the top Right neighbour process.
	@param[in]	botRight: rank of the bottom Right neighbour process.
 	@param[in] 	topRow: this is the Row from the Upper Neighbour Process.	 
 	@param[in] 	botRow: this is the Row from the bottom Neighbour Process.	 
 	@param[in] 	leftCol: this is the Row from the left Neighbour Process.	 
 	@param[in] 	rightCol: this is the Row from the right Neighbour Process.	 

*/
inline void calculate4SingleAngleCells(std::vector<std::vector<int>> &blockArrayNext, Block &blockArrayCurrent,
                                       int topLeft, int botLeft, int topRight, int botRight,
                                       std::vector<int> topRow, std::vector<int> botRow, std::vector<int> leftCol, std::vector<int> rightCol)
{

    int aliveNeighbours = 0;
    int cellsperblockRow = blockArrayCurrent.bl.noofcols;
    int cellsperblockCol = blockArrayCurrent.bl.noofrows;

    //We calculate the topLeft !
    aliveNeighbours = topRow[0] + topRow[1] + topLeft + leftCol[0] + leftCol[1] + blockArrayCurrent.blockArray[0][1] + blockArrayCurrent.blockArray[1][1] + blockArrayCurrent.blockArray[1][0];
    if (blockArrayCurrent.blockArray[0][0] == 1)
        blockArrayNext[0][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
    else
    {
        blockArrayNext[0][0] = aliveNeighbours == 3 ? 1 : 0;
    }

    aliveNeighbours = 0;
    //We calculate the topRight !
    aliveNeighbours = topRow[cellsperblockRow - 1] + topRow[cellsperblockRow - 2] + topRight + rightCol[0] + rightCol[1] + blockArrayCurrent.blockArray[0][cellsperblockRow - 2] + blockArrayCurrent.blockArray[1][cellsperblockRow - 1] + blockArrayCurrent.blockArray[1][cellsperblockRow - 2];
    if (blockArrayCurrent.blockArray[0][cellsperblockRow - 1] == 1)
        blockArrayNext[0][cellsperblockRow - 1] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
    else
    {
        blockArrayNext[0][cellsperblockRow - 1] = aliveNeighbours == 3 ? 1 : 0;
    }

    aliveNeighbours = 0;
    //We calculate the botLeft !
    aliveNeighbours = botRow[0] + botRow[1] + botLeft + leftCol[cellsperblockCol - 1] + leftCol[cellsperblockCol - 2] + blockArrayCurrent.blockArray[cellsperblockCol - 1][1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][0];
    if (blockArrayCurrent.blockArray[cellsperblockCol - 1][0] == 1)
        blockArrayNext[cellsperblockCol - 1][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
    else
    {
        blockArrayNext[cellsperblockCol - 1][0] = aliveNeighbours == 3 ? 1 : 0;
    }

    aliveNeighbours = 0;
    //We calculate the botRight
    aliveNeighbours = botRow[cellsperblockRow - 1] + botRow[cellsperblockRow - 2] + botRight + rightCol[cellsperblockCol - 1] + rightCol[cellsperblockCol - 2] + blockArrayCurrent.blockArray[cellsperblockCol - 1][cellsperblockRow - 2] + blockArrayCurrent.blockArray[cellsperblockCol - 2][cellsperblockRow - 1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][cellsperblockRow - 2];
    if (blockArrayCurrent.blockArray[cellsperblockCol - 1][cellsperblockRow - 1] == 1)
        blockArrayNext[cellsperblockCol - 1][cellsperblockRow - 1] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
    else
    {
        blockArrayNext[cellsperblockCol - 1][cellsperblockRow - 1] = aliveNeighbours == 3 ? 1 : 0;
    }
}

/*
	@brief Helper host Function for calculating the exoteric Rows
	
	@param[in]	blockArrayNext : Block of the next Generation
	@param[in]	blockArrayCurrent : Block of the current Generation
 	@param[in] 	topRow: this is the Row from the Upper Neighbour Process.	 
 	@param[in] 	botRow: this is the Row from the bottom Neighbour Process.	 

*/
inline void calculateExotericRows(std::vector<std::vector<int>> &blockArrayNext, Block &blockArrayCurrent,
                                  std::vector<int> topRow, std::vector<int> botRow)
{

    int aliveNeighbours = 0;
    int cellsperblockRow = blockArrayCurrent.bl.noofcols;
    int cellsperblockCol = blockArrayCurrent.bl.noofrows;

    //We calculate the top inbetween
    for (int i = 1; i <= cellsperblockRow - 2; i++)
    {
        aliveNeighbours = 0;
        aliveNeighbours = topRow[i - 1] + topRow[i] + topRow[i + 1] + blockArrayCurrent.blockArray[0][i - 1] + blockArrayCurrent.blockArray[0][i + 1] + blockArrayCurrent.blockArray[1][i - 1] + blockArrayCurrent.blockArray[1][i] + blockArrayCurrent.blockArray[1][i + 1];
        if (blockArrayCurrent.blockArray[0][i] == 1)
            blockArrayNext[0][i] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[0][i] = aliveNeighbours == 3 ? 1 : 0;
        }
    }

    //We calculate the bot inbetween
    for (int i = 1; i <= cellsperblockRow - 2; i++)
    {
        aliveNeighbours = 0;
        aliveNeighbours = botRow[i - 1] + botRow[i] + botRow[i + 1] + blockArrayCurrent.blockArray[cellsperblockCol - 1][i - 1] + blockArrayCurrent.blockArray[cellsperblockCol - 1][i + 1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][i - 1] + blockArrayCurrent.blockArray[cellsperblockCol - 2][i] + blockArrayCurrent.blockArray[cellsperblockCol - 2][i + 1];
        if (blockArrayCurrent.blockArray[cellsperblockCol - 1][i] == 1)
            blockArrayNext[cellsperblockCol - 1][i] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[cellsperblockCol - 1][i] = aliveNeighbours == 3 ? 1 : 0;
        }
    }
}

/*
	@brief Helper host Function for calculating the exoteric Columns
	
	@param[in]	blockArrayNext : Block of the next Generation
	@param[in]	blockArrayCurrent : Block of the current Generation
 	@param[in] 	leftCol: this is the Row from the left Neighbour Process.	 
 	@param[in] 	rightCol: this is the Row from the right Neighbour Process.	 

*/
inline void calculateExotericCols(std::vector<std::vector<int>> &blockArrayNext, Block &blockArrayCurrent,
                                  std::vector<int> leftCol, std::vector<int> rightCol)
{

    int aliveNeighbours = 0;
    int cellsperblockRow = blockArrayCurrent.bl.noofcols;
    int cellsperblockCol = blockArrayCurrent.bl.noofrows;

    //We calculate the left inbetween
    for (int i = 1; i <= cellsperblockCol - 2; i++)
    {
        aliveNeighbours = 0;
        aliveNeighbours = leftCol[i - 1] + leftCol[i] + leftCol[i + 1] + blockArrayCurrent.blockArray[i - 1][0] + blockArrayCurrent.blockArray[i + 1][0] + blockArrayCurrent.blockArray[i - 1][1] + blockArrayCurrent.blockArray[i + 1][1] + blockArrayCurrent.blockArray[i][1];
        if (blockArrayCurrent.blockArray[i][0] == 1)
            blockArrayNext[i][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[i][0] = aliveNeighbours == 3 ? 1 : 0;
        }
    }

    //We calculate the right inbetween
    for (int i = 1; i <= cellsperblockCol - 2; i++)
    {
        aliveNeighbours = 0;
        aliveNeighbours = rightCol[i - 1] + rightCol[i] + rightCol[i + 1] + blockArrayCurrent.blockArray[i - 1][cellsperblockRow - 1] + blockArrayCurrent.blockArray[i + 1][cellsperblockRow - 1] + blockArrayCurrent.blockArray[i - 1][cellsperblockRow - 2] + blockArrayCurrent.blockArray[i + 1][cellsperblockRow - 2] + blockArrayCurrent.blockArray[i][cellsperblockRow - 2];
        if (blockArrayCurrent.blockArray[i][0] == 1)
            blockArrayNext[i][0] = aliveNeighbours == 3 || aliveNeighbours == 2 ? 1 : 0;
        else
        {
            blockArrayNext[i][0] = aliveNeighbours == 3 ? 1 : 0;
        }
    }
}

/*
	@brief	Host Function for Exoteric Calculation using pure MPI

	@param[in]	blockArrayNext : Block of the next Generation as a result
	@param[in]	blockArrayCurrent : Block of the current Generation
	@param[in]	NPs : details of the Neighbour Processes
*/
inline void PMPI_exotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &blockArrayCurrent, NeighProcesses &NPs)
{
    calculate4SingleAngleCells(blockArrayNext, blockArrayCurrent, NPs.upLeftSingle, NPs.downLeftSingle, NPs.upRightSingle, NPs.downRightSingle, NPs.rcvdFromUpperNeigh, NPs.rcvdFromDownNeigh, NPs.rcvdFromLeftNeigh, NPs.rcvdFromRightNeigh);
    calculateExotericRows(blockArrayNext, blockArrayCurrent, NPs.rcvdFromUpperNeigh, NPs.rcvdFromDownNeigh);
    calculateExotericCols(blockArrayNext, blockArrayCurrent, NPs.rcvdFromLeftNeigh, NPs.rcvdFromRightNeigh);
}
