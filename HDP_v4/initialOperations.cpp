/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file initialOperations.cpp

    @brief  The implementation details of a initial Operations done by master process (rank = 0)
*/
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <time.h>
#include "initialOperations.h"

/*
  @brief This is a Host Function for reading the command line arguments and setting them to the helper object Arg

  @param[in]  Args: Helper Object for passing Command Line Arguments 
  @param[in]  argc:	The number of command-line arguments
  @param[in]  argv:	The list of command-line arguments
*/
void identifyArgs(Arguments &Args, int argc, char *argv[])
{
    int noofRows, noofCols;
    char fileOrRandom;
    string fileName;
    string operation;

    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-rows"))
        {
            noofRows = atoi(argv[i + 1]);
        }
        else if (!strcmp(argv[i], "-cols"))
        {
            noofCols = atoi(argv[i + 1]);
        }
        else if (!strcmp(argv[i], "-f"))
        {
            fileOrRandom = 'f';
            fileName = argv[i + 1];
        }
        else if (!strcmp(argv[i], "-r"))
        {
            fileOrRandom = 'r';
            fileName = "RandomArr";
        }
    }

    Args.setMembers(noofCols, noofRows, fileOrRandom, fileName, operation);
}

/*
  @brief This is a Host Function for finding grid's dimensions

  @param[in]  Args: Helper Object for Command Line Arguments 
*/
void findDimensions(Arguments &Args)
{
    int noofrows = 0;
    int noofcols = 0;
    string STRING;
    ifstream myfile(Args.filename.c_str());
    if (!myfile)
    {
        cerr << "Couldn't open the file" << endl;
        fflush(stdout);
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    getline(myfile, STRING);
    noofcols = STRING.length();
    noofrows = count(istreambuf_iterator<char>(myfile), istreambuf_iterator<char>(), '\n') + 1;

    Args.updateMembers(noofcols, noofrows);
    myfile.close();
}

/*
  @brief This is a Host Function for creating the random Grid

  @param[in]  noofColumns: number of chosen columns 
  @param[in]  noofRows: number of chosen rows 
*/
void createRandomGrid(int noofColumns, int noofRows)
{

    FILE *pFile;
    pFile = fopen("./RandomArr", "wb");
    if (pFile == NULL)
    {
        cerr << "Couldn't open the file" << endl;
        fflush(stdout);
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    srand(time(NULL));
    int num;
    char pchar[2];
    string numberstring;
    for (int i = 0; i < noofRows; i++)
    {
        for (int j = 0; j < noofColumns; j++)
        {
            num = rand() % 2;
            sprintf(pchar, "%d", num);
            fwrite(pchar, sizeof(char), 1, pFile);
        }
        fwrite("\n", sizeof(char), 1, pFile);
    }
    fclose(pFile);
}

/*
  @brief This is for testing edge cases of Dims_create

  @param[in]  ndims:  Grid's dimensions [current: 2D]
  @param[in]  dims[]: number of Processes Per Dimension (2D)
*/
int prodof(int ndims, const int dims[])
{
    int i, prod = 1;
    for (i = 0; i < ndims; i++)
        prod *= dims[i];
    return prod;
}

/*
  @brief Host function for getting values for command line and making initial operations for creating - sending/receiving initial values

  @param[in] rank:  rank after cartesian topology
  @param[in] argc:  number of cmd line arguments
  @param[in] nprocs:  number of processes
  @param[in] argv:  cmd line arguments
  @param[in] Args: helper class Arguments
*/
void getInitialValues(int rank, int argc, int nprocs, char *argv[], Arguments &Args)
{
    int details[3];
    if (rank == 0)
    {

        identifyArgs(Args, argc, argv);
        if (Args.fileOrRandom == 'r')
        {
            createRandomGrid(Args.noofColumns, Args.noofRows);
        }
        else if (Args.fileOrRandom == 'f')
        {
            findDimensions(Args);
        }

        details[0] = Args.noofColumns;
        details[1] = Args.noofRows;
        details[2] = Args.fileOrRandom;

        /*Sent to every other process*/
        for (int i = 1; i < nprocs; i++)
        {
            MPI_Send(&details[0], 3, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Send(Args.filename.c_str(), Args.filename.size(), MPI_CHAR, i, 0, MPI_COMM_WORLD);
            MPI_Send(Args.operation.c_str(), Args.operation.size(), MPI_CHAR, i, 0, MPI_COMM_WORLD);
        }
    }
    else
    {
        MPI_Status status;
        MPI_Recv(details, 6, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
        Args.noofColumns = details[0];
        Args.noofRows = details[1];
        Args.fileOrRandom = details[2];

        /*Get FileName*/
        MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
        int l;
        MPI_Get_count(&status, MPI_CHAR, &l);
        char *buf = new char[l + 1];
        MPI_Recv(buf, l, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);
        buf[l] = '\0';
        Args.filename = buf;
        delete buf;

        MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_CHAR, &l);
        char *buf2 = new char[l + 1];
        MPI_Recv(buf2, l, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);
        buf2[l] = '\0';
        Args.operation = buf2;
        delete buf2;
    }
}