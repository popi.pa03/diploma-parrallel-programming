/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file initialOperations.h
    @brief The header containing host functions of the initial operations done by master process (rank = 0)

*/
#ifndef INITIALOPERATIONS__H
#define INITIALOPERATIONS__H

#include "Arguments.h"
#include "mpi.h"

int prodof(int, const int[]);
void identifyArgs(Arguments &Args, int argc, char *argv[]);
void findDimensions(Arguments &Args);
void createRandomGrid(int noofColumns, int noofRows);
void getInitialValues(int rank, int argc, int nprocs, char *argv[], Arguments &Args);

#endif