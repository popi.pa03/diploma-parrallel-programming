/*
  PARALLEL PROGRAMMING - HYBRID USE OF MPI-OPENMP-CUDA IN GAME OF LIFE

    @file Calculations.h
    @brief The header containing host functions for Exoteric Calculations(pure MPI and Hybrid mpi-openMP) &  Termination Check

*/
#ifndef CALCULATIONS__HPP
#define CALCULATIONS__HPP

#include <vector>

#if PUREMPI_EXOTERIC__HPP

#include "pureMPI_Exoteric.hpp"

void exotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &blockArrayCurrent, NeighProcesses &NPs)
{
    PMPI_exotericCalculation(blockArrayNext, blockArrayCurrent, NPs);
}

#elif OPENMP_EXOTERIC__HPP
#include "openMP_Exoteric.hpp"
void exotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &blockArrayCurrent, NeighProcesses &NPs)
{
    openMP_exotericCalculation(blockArrayNext, blockArrayCurrent, NPs);
}

#endif

#if PUREMPI_ESOTERIC__HPP
#include "MPI_Only_Esoteric.hpp"

inline void esotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &Bl)
{
    pMPI_esotericCalculation(blockArrayNext, Bl);
}

#elif CUDA_ESOTERIC__HPP
#include "hybridCudaMPI.hpp"

inline void esotericCalculation(std::vector<std::vector<int>> &blockArrayNext, Block &Bl)
{
    cuda_esotericCalculation(blockArrayNext, Bl);
}

#endif

inline void Terminate(std::vector<std::vector<int>> &blockArrayNext, std::vector<std::vector<int>> &blockArrayCurrent, int *toSend)
{
    std::vector<std::vector<int>> zeroVec(blockArrayCurrent.size(), std::vector<int>(blockArrayCurrent[0].size(), 0));

    *toSend = ((blockArrayCurrent == zeroVec) || (blockArrayNext == blockArrayCurrent)) == true ? 1 : 0;
}
#endif
